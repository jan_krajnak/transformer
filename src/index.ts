import { List, Set, Iterable } from 'immutable'
type Data = Iterable<any, any>

export function transformErrors(data: Data, nested: List<string> = List()) {
  return data.map((value: Data, key: string) =>
    extract(value)(nested.includes(key))
  )
}

function extract(data: Data) {
  return function(nested: boolean): Data {
    if (nested) {
      return data.map(mapper)
    }
    return data.reduce(reducer, Set()).join(' ')
  }
}

function reducer(acc: Set<Data | string>, item: Data) {
  if (typeof item === 'string') {
    return acc.add(formatString(item))
  }
  return item.reduce(reducer, acc)
}

function mapper(item: Data | string): Data | string {
  if (typeof item === 'string') {
    return formatString(item)
  }

  if (List.isList(item)) {
    if (isSameType(item, 'string')) {
      return item.map(mapper).join(' ')
    }
  }

  return item.map(mapper)
}

function formatString(str: string): string {
  return `${str}.`
}

function isSameType(list: Data, kind: string): boolean {
  return list.every((i: any) => typeof i === kind)
}
