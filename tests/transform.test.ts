import { List, fromJS } from 'immutable'
import { transformErrors } from '../src'

const errors = fromJS({
  name: ['This field is required'],
  age: ['This field is required', 'Only numeric characters are allowed'],
  urls: [
    {},
    {},
    {
      site: {
        code: ['This site code is invalid'],
        id: ['Unsupported id'],
      },
    },
  ],
  url: {
    site: {
      code: ['This site code is invalid'],
      id: ['Unsupported id'],
    },
  },
  tags: [
    {},
    {
      non_field_errors: ['Only alphanumeric characters are allowed'],
      another_error: ['Only alphanumeric characters are allowed'],
      third_error: ['Third error'],
    },
    {},
    {
      non_field_error: [
        'Minimum length of 10 characters is required',
        'Only alphanumeric characters are allowed',
      ],
    },
  ],
  tag: {
    nested: {
      non_field_errors: ['Only alphanumeric characters are allowed'],
    },
  },
})

test('should transform with nested errors', () => {
  // errors for `url` and `urls` keys should be nested
  const expected = {
    name: 'This field is required.',
    age: 'This field is required. Only numeric characters are allowed.',
    urls: [
      {},
      {},
      {
        site: {
          code: 'This site code is invalid.',
          id: 'Unsupported id.',
        },
      },
    ],
    url: {
      site: {
        code: 'This site code is invalid.',
        id: 'Unsupported id.',
      },
    },
    tags: 'Only alphanumeric characters are allowed. Third error. Minimum length of 10 characters is required.',
    tag: 'Only alphanumeric characters are allowed.',
  }

  const actual = transformErrors(errors, List(['url', 'urls']))
  expect(actual.toJS()).toEqual(expected)
})

test('should transform without nested', () => {
  const expected = {
    name: 'This field is required.',
    age: 'This field is required. Only numeric characters are allowed.',
    urls: 'This site code is invalid. Unsupported id.',
    url: 'This site code is invalid. Unsupported id.',
    tags: 'Only alphanumeric characters are allowed. Third error. Minimum length of 10 characters is required.',
    tag: 'Only alphanumeric characters are allowed.',
  }

  const actual = transformErrors(errors)
  expect(actual.toJS()).toEqual(expected)
})
